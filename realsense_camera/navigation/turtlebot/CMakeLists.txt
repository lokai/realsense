cmake_minimum_required(VERSION 2.8.3)
project(realsense_navigation)
find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  turtlebot_description
  urdf
  xacro
)
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES thumperbot_description
 CATKIN_DEPENDS roscpp rospy turtlebot_description urdf xacro
#  DEPENDS system_lib
)
include_directories(
  ${catkin_INCLUDE_DIRS}
)
